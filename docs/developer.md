# Developer

### Basic How To Develop

- activate environment: `poetry shell`
    - from project root
- install app packages: `poetry install`
- run app (if you run the app - e.g. `mpb info` from the poetry shell terminal you will be using your local instance vs the globally installed instance)
- update packages: `poetry update --dry-run`
- 'clean' local dev env: `poetry env remove $(which python)`
    - this is useful after you have removed a package and you want to 'clean' things


### Publishing to Pypi

- build: `poetry build`
- publish: `poetry publish --build`
